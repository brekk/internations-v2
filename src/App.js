/** @jsx jsx */
import { jsx, css } from "@emotion/core"
import React from "react"
import blem from "blem"
import { withHandlers, withState } from "recompose"
import { isFunction, prop, map, pipe, curry, pathOr, reduce } from "f-utility"
import { trace } from "xtrace"

import { Text, Heading } from "@rebass/emotion"
import { Flex, Box } from "@rebass/grid/emotion"

const zstyle = bem => (x, y) => ({ className: y ? bem(x) : bem(x, y) })

const renderGroups = curry(
  (removeGroup, g) =>
    g && (
      <Flex flexDirection="row" key={g}>
        <Box width={0.8} p={2}>
          <Text fontSize={2}>{g}</Text>
        </Box>
        <Box width={0.2} p={2} as="button" onClick={() => removeGroup(g)}>
          &times;
        </Box>
      </Flex>
    )
)

const User = pipe(
  withHandlers({
    toggleEditMode: ({ editMode, setEditMode }) => () => setEditMode(!editMode)
  }),
  withState("editMode", "setEditMode", false)
)(
  ({
    name,
    groups,
    editMode,
    toggleEditMode,
    groupUserLookup,
    allGroups,
    removeUserFromGroup
  }) => (
    <Box
      p={2}
      mx={0}
      my={2}
      css={css`
        background-color: ${editMode ? "#fee" : "transparent"};
        border: 1px ${editMode ? "dashed" : "solid"} #888;
      `}
    >
      <Box
        css={css`
          float: right;
        `}
        as="button"
        onClick={toggleEditMode}
      >
        Edit
      </Box>
      <Heading fontSize={3} fontWeight="bold" my={2}>
        {name}
      </Heading>
      {groups.length ? <Heading fontSize={1}>Member of:</Heading> : null}
      <Box as="ul" px={4}>
        {groups &&
          map(
            g => (
              <Box as="li" key={g}>
                <Text fontSize={1}>{g}</Text>
                {editMode && (
                  <button onClick={() => removeUserFromGroup(name, g)}>
                    &times;
                  </button>
                )}
              </Box>
            ),
            groups
          )}
      </Box>
      {editMode && (
        <select>
          {map(
            o => (
              <option key={o} value={o}>
                {o}
              </option>
            ),
            allGroups.filter(f => groups.indexOf(f) === -1)
          )}
        </select>
      )}
    </Box>
  )
)

const renderUsers = curry(
  (parentProps, u) => u && <User key={u.name} {...parentProps} {...u} />
)

const addEntityToList = curry((entity, setter, compare, cleanup) => props => {
  const { [entity]: e, [setter]: s } = props
  return x => {
    if (e && e.filter(compare(x)).length === 0) {
      const f = e.concat(x)
      if (isFunction(cleanup)) cleanup(f, props, entity)
      return s(f)
    }
  }
})
const removeEntityFromList = curry(
  (entity, setter, compare, cleanup) => props => x => {
    const { [entity]: e, [setter]: s } = props
    if (x) {
      const f = e.filter(compare(x))
      if (isFunction(cleanup)) cleanup(f, props, entity)
      return s(f)
    }
  }
)

const compareDirect = curry((a, b) => a === b)
const rejectDirect = curry((a, b) => a !== b)
const compareByName = curry((x, f) => f.name === x.name)
const rejectByName = curry((x, f) => f.name !== x.name)

const deriveUserGroups = reduce((mapping, [user, groups]) => {
  groups.forEach(group => {
    if (!mapping[group]) {
      mapping[group] = [user]
    } else if (mapping[group].indexOf(user) === -1) {
      mapping[group] = mapping[group].concat(user)
    }
  })
  return mapping
}, {})

const deriveUsers = curry((rawUsers, groups, lookup) =>
  rawUsers.map(u => {
    u.groups = (u.groups || []).filter(g => ~groups.indexOf(g))
    const uGroups = lookup.filter(([name, uGroups]) => name === u.name)
    console.log("UUUUUU", u, uGroups, groups)
    if (uGroups.length > 0) {
      u.groups = uGroups[0][1]
    }
    return u
  })
)

const sideEffect = curry((fn, x) => {
  sideEffect(x)
  return x
})

const fixify = (x, props, entity) => {
  console.log("x", x, "props", props, "entity", entity)
  const derivedUsers = deriveUsers(
    props.rawUsers,
    props.groups,
    props.userGroupLookup
  )
  props.__setUsers(derivedUsers)
  console.log("users", derivedUsers)
  const derivedUserGroups = deriveUserGroups(props.userGroupLookup)
  props.__setGroupUserLookup(derivedUserGroups)
  console.log("groupUserLookup", derivedUserGroups)
}

const decorate = pipe(
  withHandlers({
    addGroup: addEntityToList("groups", "setGroups", compareDirect, fixify),
    removeGroup: removeEntityFromList(
      "groups",
      "setGroups",
      rejectDirect,
      fixify
    ),
    addUser: addEntityToList("rawUsers", "setUsers", compareByName, fixify),
    removeUser: removeEntityFromList(
      "rawUsers",
      "setUsers",
      compareByName,
      fixify
    ),
    addUserToGroup: ({ setUserGroupLookup }) => x => setUserGroupLookup(x),
    removeUserFromGroup: props => (name, group) => {
      const { userGroupLookup, setUserGroupLookup } = props
      console.log(name, `<><><><><>`, group)
      const filtered = userGroupLookup.map(([f, g]) => [
        f,
        f === name ? g.filter(h => h === group) : g
      ])
      console.log("filteeeee", filtered)
      fixify([], props, "uhhh")
      return setUserGroupLookup(filtered)
    },
    isUserInGroup: ({ userGroupLookup }) => name =>
      !!userGroupLookup.find(([x]) => name === x)
  }),
  withState("newGroup", "setNewGroup", ""),
  // __setUsers is prefixed this way because we want users to be the derived
  // form of rawUsers, which is populated with the userGroupLookup mapping
  withState("users", "__setUsers", ({ rawUsers, groups, userGroupLookup }) =>
    deriveUsers(rawUsers, groups, userGroupLookup)
  ),
  // __setGroupUserLookup is prefixed this way because groupUserLookup is a derived
  // version of userGroupLookup for ease of lookups
  withState(
    "groupUserLookup",
    "__setGroupUserLookup",
    pipe(
      prop("userGroupLookup"),
      deriveUserGroups
    )
  ),
  withState("userGroupLookup", "setUserGroupLookup", [
    ["john q.", ["a"]],
    ["susan x.", ["a", "c"]]
  ]),
  // rawUsers is the user list without any groups populated
  withState("rawUsers", "setUsers", [
    {
      name: "john q."
    },
    { name: "timothy" },
    { name: "susan x." }
  ]),
  withState("groups", "setGroups", ["a", "b", "c"])
)

const AddGroup = ({ addGroup, newGroup, setNewGroup }) => (
  <Flex>
    <Box
      width={0.8}
      p={2}
      as="input"
      type="text"
      value={newGroup}
      onChange={pipe(
        pathOr("", ["target", "value"]),
        setNewGroup
      )}
    />
    <Box
      width={0.2}
      as="button"
      onClick={() => {
        if (newGroup) {
          addGroup(newGroup)
          setNewGroup("")
        }
      }}
    >
      Add
    </Box>
  </Flex>
)

const App = ({
  bem = blem("App"),
  zz = zstyle(bem),
  groups = [],
  users = [],
  newGroup,
  // methods
  addGroup,
  removeGroup,
  addUser,
  removeUser,
  addUserToGroup,
  removeUserFromGroup,
  setNewGroup,
  userGroupLookup,
  groupUserLookup
}) => {
  console.log("gruuuupppe", userGroupLookup, groupUserLookup)
  return (
    <Flex as="section" {...zz()}>
      <Box width={[1, 1 / 3]} {...zz("groups")} p={2}>
        <Heading>Groups</Heading>
        <Box
          css={css`
            margin-top: 0.5rem;
          `}
          width={1}
        >
          {map(renderGroups(removeGroup), groups)}
        </Box>
        <AddGroup
          addGroup={addGroup}
          newGroup={newGroup}
          setNewGroup={setNewGroup}
        />
      </Box>
      <Box width={[1, 2 / 3]} {...zz("users")} p={2}>
        <Heading>Users</Heading>
        <Box width={1}>
          {map(
            renderUsers({
              removeUserFromGroup,
              groupUserLookup,
              allGroups: groups
            }),
            users
          )}
        </Box>
      </Box>
    </Flex>
  )
}

export default decorate(App)
